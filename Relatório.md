# Apostila Grupo3

No dia 28/07/2021 o grupo 3 se reuniu através do Google Meet para determinar como será o backlog (presente na aba "issues" do projeto no gitlab), cronograma (contendo as definições das entregas parciais), definição do período das Sprints (1 Semana), Scrum Master (Pedro do Amaral Gonçalves), elaboração do gitflow e por fim decidimos como será feito o sumario cujo o mesmo segue a Ementa fornecida pelo professor Rafael Emerick.

Dias de entrega parciais:
- 1° --> 09/08.
- 2° --> 16/08.
- 3° --> 23/08.
- 4° --> 30/08.

Sumário:
1. Introdução à Automação e Controle
    1.1 Introdução à automação em sistemas de produção: elementos básicos de um sistema automatizado;
    1.2 Elementos básicos de um sistema de controle; 
    1.3 Níveis de automação (pirâmide de automação);
    1.4 Introdução à manufatura computadorizada; 
    1.5 Relação entre os níveis de automação e a programabilidade de instrumentos, controladores, supervisores e gerenciadores.

2. Introdução à Conceitos Básicos de Computação
    2.1 Um breve resumo sobre a história da computação;
    2.2 Componentes básicos de um computador;
        1.2.1 Conceitos básicos de Hardware;
        1.2.2 Conceitos básicos de Software.
        1.2.3 Relação entre computadores e sistemas embarcados2.3 Representação da informação;
    2.4 Sistema de numeração;
    2.5 Organização e funcionamento do computador;
    2.6 Principais áreas da computação;

3. Pensamento Computacional
    3.1 Introdução;
    3.2 Exemplo de problemas;
    3.3 Pilares do Pensamento Computacional.
        3.3.1 Decomposição;
        3.3.2 Abstração;
        3.3.3 Reconhecimento de Padrões;
        3.3.4 Algoritmos.

4. Algoritmos
    4.1 Definição de Algoritmos;
    4.2 Introdução à Lógica de Programação;
    4.3 Variáveis e Tipos de Dados;
    4.4 Operações Básicas;
    4.5 Comandos de Entrada e Saída;
    4.6 Estrutura de Decisão;
    4.7 Estruturas de Repetição.

5. Prática de programação e Introdução à Linguagem C.
    5.1 Histórico;
    5.2 Algumas características da Linguagem C;
    5.3 Processo de Compilação;
    5.4 A Estrutura de um programa na Linguagem C.